#!/usr/bin/env python

import requests
import os
import json

sample="""
<i>Testing</i>
Sample telegram

"""



def sendMessage(message=sample):
	configfile="{}/.qttnot".format(os.environ['HOME'])
	j=json.load(open(configfile,'r'))
	url="https://api.telegram.org/bot{}/sendMessage".format(j['token'])
	print(j)

	data={
		'chat_id': j['chatid'],
		'text': message,
		'parse_mode': 'HTML'
		}
	return requests.post(url,data=data).json()



if (__name__== "__main__"):
	print(sendMessage())



